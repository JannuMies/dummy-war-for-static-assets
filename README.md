# A basic Gradle WAR that's build from two modules

A basic thing to show bundling of compiled assets from a
Gradle module to a WAR-file built in another Gradle-module.

## How to use:
 * `gradle clean war`
 * `docker-compose up`
 * Login to http://localhost:8080/manager/ with the user `manager`
 * Upload the .war file in `war-module/build/libs/war-module.war`
 * navigate to http://localhost:8080/war-module/htmlAssets/index.html
 
